# Nexi

Nexi is a list manager built as a PWA ([Progressive Web App](https://en.wikipedia.org/wiki/Progressive_web_app)).

It was first intented to track the series of books I wanted to buy.
For that, each item could have a number, to know which was the next book of the serie I wanted.
Each lists was sub-divised in two, for already published books and not yet published books.

As this kind of list management could be used for other purposes (grocery list for example),
I decided to make Nexi more purpose-agnostic and changed some terms.

## What does Nexi means?

Nexi was initially named `Next Book` because it was its purpose to help we know what was the next book of a serie I wanted.
As it was redesigned to be agnostic, I thought of `Next Item` and… *voilà*: `NEXt Item`.

## Demo

You can try on <https://nexi.fiat-tux.fr>.

## INSTALLATION

Clone the project and serve the directory with a web server. Done.

## CONTRIBUTING

Please, see the [CONTRIBUTING](CONTRIBUTING) file.

## LOGO

Many, many thanks to [@gub](https://framapiaf.org/@gub) for pimping the logo!

## LICENSE

Licensed under the terms of the AGPLv3. See [LICENSE](LICENSE) file.

## Dependencies

- [AlpineJS](https://github.com/alpinejs/alpine)
- [TailwindCSS](https://tailwindcss.com/)
- [JsStore](https://jsstore.net/)
