module.exports = {
  purge: [
      '../index.html',
      '../img/icons/*svg'
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  darkMode: 'class'
}
