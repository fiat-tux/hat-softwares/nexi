let dbName = 'NextI';

function getDbSchema (version) {
    let tblLists = {
        name: 'lists',
        columns: {
            id:   { primaryKey: true, autoIncrement: true               },
            name: { notNull: true,    dataType: 'string',  unique: true }
        }
    };
    let tblItems = {
        name: 'items',
        columns: {
            id:      { primaryKey: true, autoIncrement: true },
            name:    { notNull: true,    dataType: 'string'  },
            list_id: { notNull: true,    dataType: 'number'  },
            status:  { notNull: true,    dataType: 'string'  },
            number:  {                   dataType: 'number'  }
        }
    };
    let tblSettings = {
        name: 'settings',
        columns: {
            name: { primaryKey: true, dataType: 'string', unique: true },
            val:  { primaryKey: true, dataType: 'string'               }
        }
    };
    if (version) {
        tblSettings.version     = 2;
        tblSettings.columns.val = { dataType: 'string' };
        if (version === 3) {
            tblLists.version             = 3;
            tblLists.columns.name        = { notNull: true, dataType: 'string' };
            tblLists.columns.sort_method = { notNull: true, dataType: 'string', default: 'az' };
            tblItems.version             = 3;
            tblItems.columns.number      = { notNull: true, dataType: 'number', default: -1 };
        }
    }
    let db = {
        name: dbName,
        tables: [
            tblLists,
            tblItems,
            tblSettings
        ]
    };
    return db;
}

let connection = new JsStore.Connection(new Worker('js/jsstore.worker.min.js'));
async function initJsStore () {
    let database    = getDbSchema();
    let isDbCreated = await connection.initDb(database);
    if (isDbCreated === true) {
        console.log('db created');
        await setSetting('version', '42');
        let lists = await connection.insert({
            into: 'lists',
            values: [ { name: 'Default list' } ],
            return: true
        });
        if (lists.length > 0) {
            console.log('Default list successfully added.');
            await setSetting('currentList', lists[0].id.toString());
        }
    } else {
        console.log('db opened');
    }

    // Version 2 of database
    let allSettings = await connection.select({ from: 'settings' });
    if (await connection.initDb(getDbSchema(2))) {
        console.log('db upgraded to v2');
        await connection.insert({
            into: 'settings',
            values: allSettings
        });
    } else {
        console.log('db already upgraded to v2');
    }
    // Version 3 of database
    let allLists = await connection.select({ from: 'lists' });
    for (let i = 0; i < allLists.length; i++) {
        allLists[i].sort_method = 'az';
    }
    let allItems = await connection.select({ from: 'items' });
    for (let i = 0; i < allItems.length; i++) {
        if (typeof(allItems[i].number) === 'undefined') {
            allItems[i].number = -1;
        }
    }
    if (await connection.initDb(getDbSchema(3))) {
        console.log('db upgraded to v3');
        await connection.insert({
            into: 'lists',
            values: allLists
        });
        await connection.insert({
            into: 'items',
            values: allItems
        });
    } else {
        console.log('db already upgraded to v3');
    }
}

async function getLists() {
    return connection.select({
        from: 'lists',
        order: {
            by: 'name',
            type: 'asc'
        }
    });
}
async function getItems(status, list) {
    let order = {
        by: 'name',
        type: 'asc'
    }
    switch (list.sort_method) {
        case 'za':
            order.type = 'desc';
            break;
        case '09':
            order.by   = 'number';
            break;
        case '90':
            order.by   = 'number';
            order.type = 'desc';
            break;
    }
    return connection.select({
        from: 'items',
        where: {
            status: status,
            list_id: list.id
        },
        order: order
    });
}
async function getSetting (setting) {
    let current = await connection.select({
        from: 'settings',
        where: {
            name: setting
        }
    });
    if (current.length > 0) {
        return current[0].val;
    } else {
        return false;
    }
}
async function setSetting (setting, value) {
    let noOfRowsUpdated = await connection.insert({
        into: 'settings',
        values: [ { name: setting, val: value } ],
        upsert: true
    });
    if (noOfRowsUpdated === 0) {
        console.log('Unable to update current list record');
    }
    return noOfRowsUpdated;
}

async function initAll () {
    await initJsStore();
    let theme = await getSetting('theme');
    if (theme === 'dark' || (theme === false && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark');
    } else {
        document.documentElement.classList.remove('dark');
    }
    this.darkTheme = (theme === 'dark' || (theme === false && window.matchMedia('(prefers-color-scheme: dark)').matches));

    this.detectLang();
    this.$refs.jsOnly.removeAttribute('id');
    this.$refs.nlName.value        = null;
    this.$refs.niName.value        = null;
    this.$refs.niNumber.value      = null;
    this.$refs.niStatus.checked    = true;
    this.$refs.importFile.value    = null;
    this.$refs.newListName.value   = null;
    this.$refs.itemId.value        = null;
    this.$refs.itemStatus.value    = null;
    this.$refs.newItemName.value   = null;
    this.$refs.oldItemName.value   = null;
    this.$refs.newItemNumber.value = null;
    this.$refs.oldItemNumber.value = null;
    this.lists  = await getLists();
    let current = await getSetting('currentList');
    let confirm = await getSetting('confirm_before_delete');
    this.lists.sort((a, b) => { return a.name.localeCompare(b.name) });
    this.confirm_before_delete = (confirm === 'true') ? true : false;
    if (this.lists.length > 0 || current) {
        this.current_list = this.lists[0];
        if (current) {
            this.current_list = this.getCurrentList(parseInt(current));
        }
        this.current = await getItems('current', this.current_list);
        this.next    = await getItems('next',    this.current_list);

        this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method ) });
        this.next.sort((a, b)    => { return this.sortMethod(a, b, this.current_list.sort_method ) });
    }
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('js/nexi-sw.js');
    }
    let response = await fetch(`version.json?t=${(new Date).getTime()}`);
    if (response.ok) {
        let data = await response.json()
        if (typeof(data) !== 'undefined') {
            let version        = data.version;
            let currentVersion = parseInt(await getSetting('version')) || 0;
            await setSetting('version', `${version}`);
            if (currentVersion < version && currentVersion != 42) {
                this.closed_message_modal = false;
                this.message              = this.i18n.need_reload;
                this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                this.must_reload          = 'true';
            }
        }
    }
}

function defaultData () {
    return {
        availableLangs: [
            { code: 'en', name: 'English'  },
            { code: 'es', name: 'Español'  },
            { code: 'fr', name: 'Français' },
            { code: 'ja', name: '日本語 (にほんご)' },
            { code: 'oc', name: 'Occitan'  }
        ],
        i18n: {
            add_item: 'Add item',
            add_item_btn: 'Add item (Shorcut: Ctrl+Enter)',
            add_new_list: 'Add a new list',
            cancel: 'Cancel',
            cant_remove_only_list: 'You can’t remove the only list you have.',
            change_sort_method: 'Change sorting method',
            close: 'Close',
            confirm_before_delete_item: 'Confirm before delete item?',
            confirm_item_removal: 'Are you sure you want to remove the item "XXX"?',
            confirm_list_removal: 'Are you sure you want to remove the current list and all its items?',
            current: 'Current',
            current_state: 'State: current?',
            decrement_number: 'Decrement number',
            default_list: 'Default list',
            edit_item: 'Modify the item',
            edit_list: 'Change the name of the list',
            export: 'Export your data',
            export_import: 'Export/import',
            import: 'Import data',
            import_file: 'Your file containing data',
            import_ok: 'Your file has been successfully imported',
            increment_number: 'Increment number',
            info: 'What is Nexi?',
            list: 'List:',
            logo: 'Nexi’s logo',
            name: 'Name',
            name_taken: 'Name already taken, please choose another one',
            need_reload: 'New version available, the page will now reload.',
            new_list_name: 'Name of the new list',
            new_name: 'New name',
            nexi_is: 'Nexi is a web-based list management software whose data does not leave your browser.',
            next: 'Next',
            no_empty_name: 'The name can’t be empty',
            no_import_file: 'Please, select a file to import',
            open_menu: 'Open menu',
            number: 'Number',
            remove_item: 'Remove item',
            remove_list: 'Remove the current list and all its items',
            toggle_item_status: 'Toggle item’s status (current/next)',
            show_menu: 'Show menu',
            source_url: 'It’s a free software, licensed under the terms of the AGPLv3. The source code is on <a href="https://framagit.org/fiat-tux/hat-softwares/nexi">https://framagit.org/fiat-tux/hat-softwares/nexi</a>.',
            use_dark_theme: 'Use dark theme'
        },
        current_lang: 'en',
        current_list: {},
        rtl: false,
        closed_menu: true,
        closed_lang_menu: true,
        closed_list_menu: true,
        closed_edit_item_modal: true,
        closed_edit_list_modal: true,
        closed_new_list_modal: true,
        closed_new_item_modal: true,
        closed_export_import_modal: true,
        closed_message_modal: true,
        closed_current: false,
        closed_next: false,
        confirm_before_delete: false,
        show_source_url: false,
        darkTheme: false,
        message: null,
        must_reload: 'false',
        lists: [],
        current: [],
        next: [],
        showInfo() {
            this.closed_message_modal = false;
            this.message              = this.i18n.nexi_is;
            this.show_source_url      = true;
            this.$nextTick(() => { this.$refs.msgBtn.focus(); });
        },
        closeMessageModal(event) {
            this.closed_message_modal = true;
            this.show_source_url      = false;
            this.enableButtons();
            if (event.target.getAttribute('data-reload') === 'true') {
                window.location.reload(true);
            }
        },
        addListModal() {
            this.closed_new_list_modal = false;
            this.$nextTick(() => { this.$refs.nlName.focus(); });
        },
        addList() {
            this.disableButtons();

            let name = this.$refs.nlName.value;
            if (name) {
                connection.insert({
                    into: 'lists',
                    values: [ { name: name } ],
                    return: true
                }).then(async (newList) => {
                    if (newList.length > 0 && typeof(newList[0].id) !== 'undefined') {
                        console.log(`List ${name} successfully added.`);

                        this.current_list = newList[0];
                        this.current      = [];
                        this.next         = [];
                        this.closed_menu  = true;
                        this.closeModal();
                        this.lists.push(newList[0]);
                        this.lists.sort((a, b) => { return a.name.localeCompare(b.name) });
                        this.$refs.nlName.value = null;
                        await setSetting('currentList', newList[0].id.toString());
                    } else {
                        console.error(`List ${name} fucked up.`);
                    }
                    this.enableButtons();
                }).catch(spitJsStoreErr);
            } else if (name === '') {
                this.enableButtons();
                this.closed_message_modal = false;
                this.message              = this.i18n.no_empty_name;
                this.$nextTick(() => { this.$refs.msgBtn.focus(); });
            } else {
                this.enableButtons();
                console.error(`The name of the new list is fucked up.`);
            }
        },
        editListModal() {
            this.closed_edit_list_modal = false;
            this.$nextTick(() => { this.$refs.newListName.focus(); });
        },
        editList() {
            this.disableButtons();

            let new_name = this.$refs.newListName.value;
            if (new_name !== null && new_name !== '' && new_name !== this.current_list.name) {
                connection.select({
                    from: 'lists',
                    where: {
                        name: new_name
                    }
                }).then(async (name_taken) => {
                    if (name_taken.length === 0) {
                        this.current_list.name = new_name;
                        this.closeModal();
                        await connection.update({
                            in: 'lists',
                            set: {
                                name: new_name
                            },
                            where: {
                                id: this.current_list.id
                            }
                        });
                    } else {
                        this.closed_message_modal = false;
                        this.message              = this.i18n.name_taken;
                        this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                    }
                    this.enableButtons();
                    this.lists.sort((a, b) => { return a.name.localeCompare(b.name) });
                }).catch(spitJsStoreErr);
            } else {
                this.enableButtons();
            }
        },
        showDownloadListModal() {
            this.closed_download_list_modal = false;
            let markdownDocument = `# ${this.i18n.current}\n`;
            for (let i = 0; i < this.current.length; i++) {
                let item = this.current[i]
                markdownDocument += `\n- ${item.name}`
                if (item.number > -1) {
                    markdownDocument += ` (${item.number.toString()})`;
                }
            }
            markdownDocument += `\n\n# ${this.i18n.next}\n`;
            for (let i = 0; i < this.next.length; i++) {
                let item = this.next[i]
                markdownDocument += `\n- ${item.name}`
                if (item.number > -1) {
                    markdownDocument += ` (${item.number.toString()})`;
                }
            }
            let exportFile  = new Blob([markdownDocument], {type : 'text/markdown'});
            let url         = window.URL.createObjectURL(exportFile);

            this.$refs.exportDataJson.href = url;
            this.$refs.exportDataJson.download = `${this.current_list.name}.md`;
            this.$refs.exportDataJson.click();
        },
        removeList() {
            if (this.lists.length > 1) {
                let confirm = window.confirm(this.i18n.confirm_list_removal);
                let old_list_id = this.current_list.id;
                if (confirm) {
                    connection.remove({
                        from: 'lists',
                        where: {
                            id: old_list_id
                        }
                    }).then(async (rowsDeleted) => {
                        if (rowsDeleted > 0) {
                            let new_cur_list  = (this.lists[0].id !== old_list_id) ? this.lists[0] : this.lists[1];
                            let index         = this.lists.findIndex((list) => parseInt(list.id) === old_list_id );
                            let removed_list  = this.lists.splice(index, 1);
                            this.current_list = this.getCurrentList(new_cur_list.id);
                            this.current      = await getItems('current', this.current_list);
                            this.next         = await getItems('next',    this.current_list);
                            await setSetting('currentList', this.current_list.id.toString());
                            await connection.remove({
                                from: 'items',
                                where: {
                                    list_id: old_list_id
                                }
                            });
                            console.log(`Current list successfully deleted (id: ${this.current_list.id})`);
                        } else {
                            console.error(`Error while trying to delete current list (id: ${this.current_list.id})`);
                        }
                    }).catch(spitJsStoreErr);
                }
            } else {
                this.closed_message_modal = false;
                this.message              = this.i18n.cant_remove_only_list;
                this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                console.error(this.i18n.cant_remove_only_list);
            }
        },
        sortMethod(a, b, sort_method) {
            switch (sort_method) {
                case 'az':
                    var nameA = a.name.toUpperCase();
                    var nameB = b.name.toUpperCase();
                    return(nameA.localeCompare(nameB));
                    break;
                case 'za':
                    var nameA = a.name.toUpperCase();
                    var nameB = b.name.toUpperCase();
                    return(nameB.localeCompare(nameA));
                    break;
                case '09':
                    var diff = a.number - b.number;
                    if (diff !== 0) {
                        return diff;
                    } else {
                        var nameA = a.name.toUpperCase();
                        var nameB = b.name.toUpperCase();
                        return(nameA.localeCompare(nameB));
                    }
                    break;
                case '90':
                    var diff = b.number - a.number;
                    if (diff !== 0) {
                        return diff;
                    } else {
                        var nameA = a.name.toUpperCase();
                        var nameB = b.name.toUpperCase();
                        return(nameA.localeCompare(nameB));
                    }
                    break;
            }
        },
        changeSortMethod() {
            let new_method = 'az';
            switch (this.current_list.sort_method) {
                case 'az':
                    new_method = 'za';
                    break;
                case 'za':
                    new_method = '09';
                    break;
                case '09':
                    new_method = '90';
                    break;
            }
            connection.update({
                in: 'lists',
                set: {
                    sort_method: new_method
                },
                where: {
                    id: this.current_list.id
                }
            }).then(async () => {
                this.current_list.sort_method = new_method;
                this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method ) });
                this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method ) });
            }).catch(spitJsStoreErr);
        },
        addItemModal() {
            this.closed_new_item_modal = false;
            this.$nextTick(() => { this.$refs.niName.focus(); });
        },
        addItem() {
            this.disableButtons();

            let name   = this.$refs.niName;
            let number = this.$refs.niNumber;
            let status = this.$refs.niStatus;
            let value = {
                name:    name.value,
                list_id: this.current_list.id,
                status:  (status.checked) ? 'current' : 'next'
            };
            if (number.value !== '') {
                value.number = parseInt(number.value);
            }
            connection.insert({
                into: 'items',
                values: [ value ],
                return: true
            }).then(async (item) => {
                this.closeModal();
                if (item[0].status === 'current') {
                    this.current.push(item[0]);
                    this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                } else {
                    this.next.push(item[0]);
                    this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                }
                this.enableButtons();
                name.value     = null;
                number.value   = null;
                status.checked = true;
            }).catch(spitJsStoreErr);
        },
        changeConfirmBeforeDelete(event) {
            setSetting('confirm_before_delete', this.confirm_before_delete.toString());
        },
        changeDarkTheme(event) {
            setSetting('theme', (this.darkTheme) ? 'dark' : 'light');
            if (this.darkTheme) {
                document.documentElement.classList.add('dark');
            } else {
                document.documentElement.classList.remove('dark');
            }
        },
        editItemModal(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            this.$refs.itemId.value        = node.getAttribute('data-id');
            this.$refs.itemStatus.value    = node.getAttribute('data-status');
            this.$refs.newItemName.value   = node.getAttribute('data-name');
            this.$refs.oldItemName.value   = node.getAttribute('data-name');
            this.$refs.newItemNumber.value = node.getAttribute('data-nb');
            this.$refs.oldItemNumber.value = node.getAttribute('data-nb');
            this.closed_edit_item_modal    = false;
            this.$nextTick(() => { this.$refs.newItemName.focus(); });
        },
        editItem() {
            this.disableButtons();

            let id      = parseInt(this.$refs.itemId.value);
            let name    = this.$refs.oldItemName.value;
            let number  = this.$refs.oldItemNumber.value;
            let status  = this.$refs.itemStatus.value;

            let new_name   = this.$refs.newItemName.value;
            let new_number = this.$refs.newItemNumber.value;
            if (new_name !== null && new_name !== '' && (new_name !== name || new_number !== number)) {
                connection.update({
                    in: 'items',
                    set: {
                        name: new_name,
                        number: parseInt(new_number)
                    },
                    where: {
                        id: id
                    }
                }).then(async (nbRowsUpdated) => {
                    if (status === 'current') {
                        let index = this.current.findIndex((item) => parseInt(item.id) === id );
                        this.current[index].name = new_name;
                        this.current[index].number = new_number;
                        this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method ) });
                    } else {
                        let index = this.next.findIndex((item) => parseInt(item.id) === id );
                        this.next[index].name = new_name;
                        this.next[index].number = new_number;
                        this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method ) });
                    }
                    this.closeModal();
                    this.enableButtons();
                }).catch(spitJsStoreErr);
            } else {
                this.enableButtons();
            }
        },
        toggleItemStatus(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let status  = node.getAttribute('data-wannabe-status');
            connection.update({
                in: 'items',
                set: { status: status },
                where: { id: item_id }
            }).then(async (noOfRowsUpdated) => {
                if (noOfRowsUpdated > 0) {
                    if (status === 'next') {
                        let index = this.current.findIndex((item) => parseInt(item.id) === item_id );
                        let item  = this.current.splice(index, 1);
                        this.next.push(item[0]);
                        this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                    } else {
                        let index = this.next.findIndex((item) => parseInt(item.id) === item_id );
                        let item  = this.next.splice(index, 1);
                        this.current.push(item[0]);
                        this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                    }
                } else {
                    console.error(`Couldn’t toggle item with id ${item_id}`);
                }
            }).catch(spitJsStoreErr);
        },
        incrementItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let number  = parseInt(node.getAttribute('data-nb')) + 1;
            let status  = node.getAttribute('data-status');
            connection.update({
                in: 'items',
                set: { number: number },
                where: { id: item_id }
            }).then(async (noOfRowsUpdated) => {
                if (noOfRowsUpdated > 0) {
                    console.log(`Item successfully upgraded (id: ${item_id})`);

                    if (status === 'current') {
                        let index = this.current.findIndex((item) => parseInt(item.id) === item_id );
                        this.current[index].number = number;
                        this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                    } else {
                        let index = this.next.findIndex((item) => parseInt(item.id) === item_id );
                        this.next[index].number = number;
                        this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                    }
                } else {
                    console.error(`Couldn’t upgrade item with id ${item_id} to next number`);
                }
            }).catch(spitJsStoreErr);
        },
        decrementItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let number  = parseInt(node.getAttribute('data-nb'));
            if (number > -1) {
                number = number - 1;
                let status  = node.getAttribute('data-status');
                connection.update({
                    in: 'items',
                    set: { number: number },
                    where: { id: item_id }
                }).then(async (noOfRowsUpdated) => {
                    if (noOfRowsUpdated > 0) {
                        console.log(`Item successfully upgraded (id: ${item_id})`);

                        if (status === 'current') {
                            let index = this.current.findIndex((item) => parseInt(item.id) === item_id );
                            this.current[index].number = number;
                            this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                        } else {
                            let index = this.next.findIndex((item) => parseInt(item.id) === item_id );
                            this.next[index].number = number;
                            this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                        }
                    } else {
                        console.error(`Couldn’t upgrade item with id ${item_id} to next number`);
                    }
                }).catch(spitJsStoreErr);
            }
        },
        removeItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let status  = node.getAttribute('data-status');
            let confirm = true;
            if (this.confirm_before_delete) {
                confirm = window.confirm(this.i18n.confirm_item_removal.replace('XXX', node.getAttribute('data-name')));
            }
            if (confirm) {
                connection.remove({
                    from: 'items',
                    where: {
                        id: item_id
                    }
                }).then(async (rowsDeleted) => {
                    if (rowsDeleted > 0) {
                        console.log(`Item successfully deleted (id: ${item_id})`);

                        if (status === 'current') {
                            let index = this.current.findIndex((item) => parseInt(item.id) === item_id );
                            let item  = this.current.splice(index, 1)[0];
                            this.current.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                        } else {
                            let index = this.next.findIndex((item) => parseInt(item.id) === item_id );
                            let item  = this.next.splice(index, 1)[0];
                            this.next.sort((a, b) => { return this.sortMethod(a, b, this.current_list.sort_method )});
                        }
                    } else {
                        console.error(`Error while trying to delete item (id: ${item_id})`);
                    }
                }).catch(spitJsStoreErr);
            }
        },
        changeSelectedList(event) {
            let list_id = parseInt(event.target.getAttribute('data-value'));
            setSetting('currentList', list_id.toString()).then(async (updated) => {
                if (updated > 0) {
                    this.current_list     = this.getCurrentList(list_id);
                    this.current          = await getItems('current', this.current_list);
                    this.next             = await getItems('next',    this.current_list);
                    this.closed_list_menu = true;
                    this.closed_menu      = true;
                }
            }).catch(spitJsStoreErr);
        },
        changeSelectedLang(event) {
            this.translate(event.target.getAttribute('data-value'));
            this.closed_lang_menu = true;
        },
        detectLang() {
            let languages = [];
            getSetting('lang').then((clang) => {
                if (clang) {
                    languages.push(clang);
                }
                let nl        = navigator.languages;
                for (let i = 0; i < nl.length; i++) {
                    languages.push(nl[i]);
                }
                if (languages.length !== 0) {
                    this.translate(languages.shift(), languages);
                } else {
                    console.log('Using old style language detection.');
                    let userLang = navigator.language || navigator.userLanguage;
                    this.translate(userLang);
                }
            }).catch((err) => {
                this.closed_message_modal = false;
                this.message              = err;
                this.$nextTick(() => { this.$refs.msgBtn.focus(); });
            });
        },
        translate(userLang, languages) {
            fetch('locales/'+userLang+'.json').then((response) => {
                if (!response.ok) {
                    if (typeof(languages) !== 'undefined' && languages !== null && languages.length !== 0) {
                        return this.translate(languages.shift(), languages);
                    }
                }
                return response.json();
            }).then(async (data) => {
                if (typeof(data) !== 'undefined') {
                    var t = this;
                    Object.keys(data).forEach(function(key) {
                        t.i18n[key] = data[key];
                    });
                    this.current_lang = userLang;
                    if (['ar', 'fa', 'he', 'ur', 'yi'].includes(userLang.substr(0, 2))) {
                        this.rtl = true;
                        document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
                    } else if (this.rtl) {
                        this.rtl = false;
                        document.getElementsByTagName('html')[0].removeAttribute('dir');
                    }
                    await setSetting('lang', userLang);
                    document.getElementsByTagName('html')[0].setAttribute('lang', userLang);
                }
            }).catch((err) => {
                console.log(`Could not download locales/${userLang}.json. Surely not translated in that language.`);
            });
        },
        currentLang () {
            let langs = this.availableLangs.filter((lang) => { return lang.code === this.current_lang });
            if (langs.length > 0 && typeof(langs[0].name) !== 'undefined') {
                return langs[0].name;
            }
            return '';
        },
        getCurrentList(id) {
            if (this.lists.length < 1) {
                return '';
            } else {
                let currentLists = this.lists.filter((list) => { return parseInt(list.id) === id });
                if (currentLists.length > 0 && typeof(currentLists[0].name) !== 'undefined') {
                    return currentLists[0];
                }
                return '';
            }
        },
        closeModal() {
            if (!this.closed_new_list_modal || !this.closed_new_item_modal || !this.closed_export_import_modal || !this.closed_edit_list_modal || !this.closed_edit_item_modal || !this.closed_message_modal) {
                this.closed_new_list_modal      = true;
                this.closed_new_item_modal      = true;
                this.closed_export_import_modal = true;
                this.closed_edit_list_modal     = true;
                this.closed_edit_item_modal     = true;
                this.closed_message_modal       = true;
                this.show_source_url            = false;
                this.$refs.nlName.value         = null;
                this.$refs.niName.value         = null;
                this.$refs.niNumber.value       = null;
                this.$refs.niStatus.checked     = true;
                this.$refs.newListName.value    = null;
                this.$refs.itemId.value         = null;
                this.$refs.itemStatus.value     = null;
                this.$refs.newItemName.value    = null;
                this.$refs.oldItemName.value    = null;
                this.$refs.newItemNumber.value  = null;
                this.$refs.oldItemNumber.value  = null;
                this.enableButtons();
            }
        },
        exportImportModal() {
            this.closed_export_import_modal = false;
            this.$nextTick(() => { this.$refs.exportData.focus(); });
        },
        exportData() {
            this.disableButtons();
            connection.select({ from: 'lists' }).then(async (lists) => {
                let items       = await connection.select({ from: 'items' });
                let storageData = [JSON.stringify({ lists: lists, items: items })];
                let exportFile  = new Blob(storageData, {type : 'application/json'});
                let url         = window.URL.createObjectURL(exportFile);

                this.$refs.exportDataJson.href = url;
                this.$refs.exportDataJson.download = 'nexi-export-data.json';
                this.$refs.exportDataJson.click();
                this.enableButtons();
            });
        },
        importData() {
            this.disableButtons();
            let f = this.$refs.importFile.files;
            if (f.length === 0) {
                this.closed_message_modal = false;
                this.message              = this.i18n.no_import_file;
                this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                return;
            }
            let reader   = new FileReader();
            let tempThis = this;
            reader.addEventListener("loadend", async () => {
                try {
                    let data         = JSON.parse(reader.result);
                    let failed_lists = [];
                    let failed_items = [];
                    let mapped_id    = {};
                    for (let i = 0; i < data.lists.length; i++) {
                        let list   = data.lists[i];
                        let old_id = list.id;
                        delete list.id;
                        let results = await connection.select({
                            from: 'lists',
                            where: {
                                name: list.name
                            }
                        });
                        if (results.length > 0) {
                            mapped_id[old_id] = results[0].id;
                        } else {
                            let inserted = await connection.insert({
                                into: 'lists',
                                values: [ { name: list.name } ],
                                return: true
                            });
                            if (inserted.length > 0) {
                                mapped_id[old_id] = inserted[0].id;
                            } else {
                                failed_lists.push(list.name);
                            }
                        }
                    }
                    for (let i = 0; i < data.items.length; i++) {
                        let item = data.items[i];
                        if (typeof(mapped_id[item.list_id]) === 'undefined') {
                            return;
                        }

                        delete item.id;
                        item.list_id = mapped_id[item.list_id];
                        let inserted = await connection.insert({
                            into: 'items',
                            values: [ item ],
                            return: true
                        });
                        if (inserted.length === 0) {
                            failed_items.push(item.name);
                        }
                    }
                    tempThis.lists   = await getLists();
                    if (failed_lists.length === 0 && failed_items.length === 0) {
                        this.closed_message_modal = false;
                        this.message              = tempThis.i18n.import_ok;
                        this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                    } else {
                        let msg = tempThis.i18n.failed_imports + '\n';
                        if (failed_lists.length !== 0) {
                            msg = msg + tempThis.i18n.lists + '\n';
                            failed_lists.forEach((list) => {
                                msg = msg + '- ' + list + '\n';
                            });
                        }
                        if (failed_items.length !== 0) {
                            msg = msg + tempThis.i18n.items + '\n';
                            failed_items.forEach((item) => {
                                msg = msg + '- ' + item + '\n';
                            });
                        }
                        this.closed_message_modal = false;
                        this.message              = msg;
                        this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                    }
                    tempThis.closeModal();
                    tempThis.enableButtons();
                } catch(err) {
                    tempThis.enableButtons();
                    this.closed_message_modal = false;
                    this.message              = err;
                    this.$nextTick(() => { this.$refs.msgBtn.focus(); });
                }
            });
            reader.readAsText(f[0]);
        },
        disableButtons() {
            let btns = document.getElementsByClassName('btn');
            for (let btn of btns) {
                btn.setAttribute('disabled', 'disabled');
            }
        },
        enableButtons() {
            let btns = document.getElementsByClassName('btn');
            for (let btn of btns) {
                btn.removeAttribute('disabled');
            }
        }
    };
}

function spitJsStoreErr (err) {
    console.error(`${err.type}! ${err.message}`);
}
