let cacheName = 'Nexi';
let contentToCache = [
    '../index.html',
    '../css/nexi.css',
    '../css/nexi.min.css',
    '../img/logo-nexi.png',
    '../img/logo-nexi.svg',
    '../js/alpine.min.js',
    '../js/jsstore.min.js',
    '../js/jsstore.worker.min.js',
    '../js/nexi.js',
    '../locales/en.json',
    '../locales/es.json',
    '../locales/fr.json',
    '../locales/ja.json',
    '../locales/oc.json'
];
self.addEventListener('install', function(e) {
    console.log('[Service Worker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[Service Worker] Caching all: app shell and content');
            return cache.addAll(contentToCache);
        })
    );
});
self.addEventListener('fetch', function(e) {
    e.respondWith(
        caches.match(e.request).then(function(r) {
            console.log('[Service Worker] Fetching resource: '+e.request.url);
            return r || fetch(e.request).then(function(response) {
                return caches.open(cacheName).then(function(cache) {
                    console.log('[Service Worker] Caching new resource: '+e.request.url);
                    cache.put(e.request, response.clone());
                    return response;
                });
            });
        })
    );
});
